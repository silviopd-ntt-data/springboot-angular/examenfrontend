package com.example.api.Service;

import com.example.api.Entity.Movie;

import java.util.List;

public interface MovieService {

    //CRUD
    public List<Movie> listar();

    public Boolean registrar(Movie movie);

    public Boolean modificar(Movie movie);

    public Boolean eliminar(Long id);

}
