package com.example.api.Service;

import com.example.api.Entity.Usuario;
import com.example.api.Repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioServiceImpl implements UsuarioService {
    
    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    public Boolean iniciarSesion(Usuario usuario) {
        Usuario usuarioOptional = usuarioRepository.iniciarSesion(usuario.getUsername(), usuario.getPassword());
        return usuarioOptional != null;
    }
}
