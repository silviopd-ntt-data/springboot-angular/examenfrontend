package com.example.api.Service;

import com.example.api.Entity.Movie;
import com.example.api.Repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MovieServiceImpl implements MovieService {

    @Autowired
    private MovieRepository movieRepository;


    @Override
    public List<Movie> listar() {
        try {
            return movieRepository.findAll();
        } catch (Exception ex) {
            throw new RuntimeException("Error al listar");
        }
    }

    @Override
    public Boolean registrar(Movie movie) {
        try {
            movieRepository.save(movie);
            return true;
        } catch (Exception ex) {
            throw new RuntimeException("Error al registrar");
        }
    }

    @Override
    public Boolean modificar(Movie movie) {
        try {
            Optional<Movie> movieOptional = movieRepository.findById(movie.getId());
            if (movieOptional.isPresent()) {
                movieRepository.save(movie);
                return true;
            }
        } catch (Exception ex) {
            throw new RuntimeException("Error al modificar el registro");
        }
        return false;
    }

    @Override
    public Boolean eliminar(Long id) {
        try {
            movieRepository.deleteById(id);
            return true;
        } catch (Exception ex) {
            throw new RuntimeException("Error al eliminar el registro");
        }
    }
}

