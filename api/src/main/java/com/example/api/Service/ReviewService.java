package com.example.api.Service;

import com.example.api.Dto.ReviewDto;
import com.example.api.Entity.Review;

import java.util.List;

public interface ReviewService {

    //CRUD
    public List<Review> listar();

    public Boolean registrar(ReviewDto review);

    public Boolean modificar(ReviewDto review);

    public Boolean eliminar(Long id);
}
