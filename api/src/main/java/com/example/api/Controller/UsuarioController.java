package com.example.api.Controller;

import com.example.api.Dto.ResponseDto;
import com.example.api.Dto.ReviewDto;
import com.example.api.Entity.Usuario;
import com.example.api.Service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/usuario")
@CrossOrigin(origins = "*")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @PostMapping
    public ResponseEntity<?> iniciarSesion(@RequestBody Usuario usuario) {
        try {
            boolean iniciarSesion = usuarioService.iniciarSesion(usuario);
            return ResponseDto.responseEntity(iniciarSesion ? "Usuario Correcto" : "", 200, iniciarSesion);
        } catch (Exception e) {
            return ResponseDto.responseEntity(e.getMessage(), 500, "");
        }
    }
}
