package com.example.api.Controller;

import com.example.api.Dto.ResponseDto;
import com.example.api.Entity.Movie;
import com.example.api.Service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/movies")
@CrossOrigin(origins = "*")
public class MovieController {

    @Autowired
    MovieService movieService;

    @GetMapping
    public ResponseEntity<?> getMovies() {
        try {
            List<Movie> movies = movieService.listar();
            return ResponseDto.responseEntity("", 200, movies);
        } catch (Exception e) {
            return ResponseDto.responseEntity(e.getMessage(), 500, "");
        }
    }

    @PostMapping
    public ResponseEntity<?> createMovie(@RequestBody Movie movie) {
        try {
            boolean create = movieService.registrar(movie);
            return ResponseDto.responseEntity(create ? "Agregado correctamente" : "", create ? 200 : 404, create ? movie : "");
        } catch (Exception e) {
            return ResponseDto.responseEntity(e.getMessage(), 500, "");
        }
    }

    @PutMapping
    public ResponseEntity<?> updateMovie(@RequestBody Movie movie) {
        try {
            boolean update = movieService.modificar(movie);
            return ResponseDto.responseEntity(update ? "Modificado correctamente" : "", update ? 200 : 404, update ? movie : "");
        } catch (Exception e) {
            return ResponseDto.responseEntity(e.getMessage(), 500, "");
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteMovie(@PathVariable Long id) {
        try {
            boolean deleted = movieService.eliminar(id);
            return ResponseDto.responseEntity(deleted ? "Pelicula eliminada" : "", deleted ? 200 : 404, "");
        } catch (Exception e) {
            return ResponseDto.responseEntity(e.getMessage(), 500, "");

        }
    }


}
