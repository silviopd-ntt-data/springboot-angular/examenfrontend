package com.example.api.Controller;

import com.example.api.Dto.ResponseDto;
import com.example.api.Dto.ReviewDto;
import com.example.api.Entity.Review;
import com.example.api.Service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/reviews")
@CrossOrigin(origins = "*")
public class ReviewController {

    @Autowired
    private ReviewService reviewService;

    @GetMapping
    public ResponseEntity<?> getReviews() {
        try {
            List<Review> reviews = reviewService.listar();
            return ResponseDto.responseEntity("", 200, reviews);
        } catch (Exception e) {
            return ResponseDto.responseEntity(e.getMessage(), 500, "");
        }
    }

    @PostMapping
    public ResponseEntity<?> createReview(@RequestBody ReviewDto review) {
        try {
            boolean create = reviewService.registrar(review);
            return ResponseDto.responseEntity(create ? "Agregado correctamente" : "", create ? 200 : 404, create ? review : "");
        } catch (Exception e) {
            return ResponseDto.responseEntity(e.getMessage(), 500, "");
        }
    }

    @PutMapping
    public ResponseEntity<?> updateReview(@RequestBody ReviewDto review) {
        try {
            boolean update = reviewService.modificar(review);
            return ResponseDto.responseEntity(update ? "Modificado correctamente" : "", update ? 200 : 404, update ? review : "");
        } catch (Exception e) {
            return ResponseDto.responseEntity(e.getMessage(), 500, "");
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteReview(@PathVariable Long id) {
        try {
            boolean deleted = reviewService.eliminar(id);
            return ResponseDto.responseEntity(deleted ? "Review eliminada" : "", deleted ? 200 : 404, "");
        } catch (Exception e) {
            return ResponseDto.responseEntity(e.getMessage(), 500, "");

        }
    }
}
