package com.example.api.Repository;

import com.example.api.Entity.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

    //    select * from usuario where username like 'silviopd' and password like '123'
    @Query(value = "select * from usuario where username like ?1 and password like ?2", nativeQuery = true)
    public Usuario iniciarSesion(String usuario, String password);

}
