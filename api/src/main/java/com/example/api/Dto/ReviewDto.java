package com.example.api.Dto;

import com.example.api.Entity.Movie;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
public class ReviewDto {


    private Long id;
    private String comment, commentator;
    private Date createAt;
    private Long movieId;
}
