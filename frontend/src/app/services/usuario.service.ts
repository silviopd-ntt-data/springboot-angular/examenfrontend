import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Review} from "../interface/review";
import {Usuario} from "../interface/usuario";

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {


  private baseUrl = environment.baseUrl;
  private relativePath: string = "/usuario";

  constructor(private http: HttpClient) {
  }

  iniciarSesion(data: Usuario): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}${this.relativePath}`, data);
  }
}
