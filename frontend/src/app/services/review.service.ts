import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Review} from "../interface/review";

@Injectable({
  providedIn: 'root'
})
export class ReviewService {

  private baseUrl = environment.baseUrl;
  private relativePath: string = "/reviews";

  constructor(private http: HttpClient) {
  }

  listReview(): Observable<Array<Review>> {
    return this.http.get<Array<Review>>(`${this.baseUrl}${this.relativePath}`);
  }

  deleteReview(id: Number | undefined): Observable<any> {
    return this.http.delete<Review>(`${this.baseUrl}${this.relativePath}/${id}`);
  }

  saveOrUpdateReview(review: Review) {
    if (review.id) {
      console.log("update");
      return this.http.put<Review>(`${this.baseUrl}${this.relativePath}`, review);
    } else {
      console.log("save");
      return this.http.post<Review>(`${this.baseUrl}${this.relativePath}`, review);
    }
  }
}
