import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {DialogMovieComponent} from "./dialogMovie/dialogMovie.component";
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';

import {MovieService} from "../../services/movie.service";
import {Movie} from "../../interface/movie";

import Swal from 'sweetalert2'

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.scss']
})
export class MoviesComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'description', 'premiereDate', 'premiere', 'actions'];
  dataSource: MatTableDataSource<Movie> = new MatTableDataSource();

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;


  constructor(private dialog: MatDialog, private movieService: MovieService) {
  }

  ngOnInit(): void {
    this.listMovie()
  }

  openDialog() {
    let settings = {
      titleModal: 'Agregar película',
      buttonModal: 'Agregar',
    }

    this.dialog.open(DialogMovieComponent, {
      width: '30%',
      data: settings
    }).afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      result ? this.listMovie() : null
    })
  }

  public listMovie() {
    this.movieService.listMovie().subscribe({
      next: (movies: any) => {
        let {data} = movies;
        // console.log(movies);
        // console.log(movies.data);
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error: (error) => {
        // console.log(error)
      },
    })
  }

  editMovie(movie: Movie) {
    let settings = {
      titleModal: 'Editar película',
      buttonModal: 'Editar',
    }

    this.dialog.open(DialogMovieComponent, {
      width: '30%',
      data: {...movie, ...settings}
    }).afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      result ? this.listMovie() : null
    })
  }

  deleteMovie(movie: Movie) {

    Swal.fire({
      title: '¿Estás seguro?',
      text: `¡No podrás revertir esta acción!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Eliminar'
    }).then((result) => {
      if (result.value) {
        this.movieService.deleteMovie(movie.id).subscribe({
          next: (movies: any) => {
            // console.log(movies);
            this.listMovie()
          },
          error: (error) => {
            // console.log(error)
          }
        })

        Swal.fire('Deleted!', 'Your file has been deleted.', 'success')
      }
    })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
