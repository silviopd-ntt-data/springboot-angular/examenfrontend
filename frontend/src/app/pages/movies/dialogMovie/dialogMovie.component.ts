import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MovieService} from "../../../services/movie.service";
import {MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

@Component({
  selector: 'app-dialog',
  templateUrl: './dialogMovie.component.html',
  styleUrls: ['./dialogMovie.component.scss']
})
export class DialogMovieComponent implements OnInit {

  isPremiere = [{
    "name": "Estreno",
    "value": true
  }, {
    "name": "No estreno",
    "value": false
  }];

  movieForm !: FormGroup

  titleModal: string = '';
  buttonModal: string = '';

  constructor(
    private formBuilder: FormBuilder,
    private apiMovie: MovieService,
    private dialogRef: MatDialogRef<DialogMovieComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
  }

  ngOnInit(): void {
    this.movieForm = this.formBuilder.group({
      id: [''],
      name: ['', Validators.required],
      description: ['', Validators.required],
      premiere: ['', Validators.required],
      premiereDate: ['', Validators.required],
    })

    console.log(this.data)
    if (this.data) {
      this.movieForm.controls['id'].setValue(this.data.id)
      this.movieForm.controls['name'].setValue(this.data.name)
      this.movieForm.controls['description'].setValue(this.data.description)
      this.movieForm.controls['premiereDate'].setValue(this.data.premiereDate)
      this.movieForm.controls['premiere'].setValue(this.data.premiere)
    }

    this.titleModal = this.data.titleModal
    this.buttonModal = this.data.buttonModal
  }

  addMovie() {
    console.log(this.movieForm.value)
    if (this.movieForm.valid) {
      this.apiMovie.saveOrUpdateMovie(this.movieForm.value).subscribe(
        {
          next: (res) => {
            // console.log(res)
            // this.movieForm.reset()
            this.dialogRef.close(true)
          },
          error: (err) => {
            // console.log(err)
          }
        }
      )
    } else {
      this.dialogRef.close(false)
    }
  }

}
