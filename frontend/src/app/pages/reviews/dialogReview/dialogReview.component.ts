import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ReviewService} from "../../../services/review.service";
import {MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {Movie} from "../../../interface/movie";


@Component({
  selector: 'app-dialog',
  templateUrl: './dialogReview.component.html',
  styleUrls: ['./dialogReview.component.scss']
})
export class DialogReviewComponent implements OnInit {

  reviewForm !: FormGroup

  titleModal: string = '';
  buttonModal: string = '';
  movieList: Array<Movie> = [];

  constructor(private formBuilder: FormBuilder,
              private apiReview: ReviewService,
              private dialogRef: MatDialogRef<DialogReviewComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit(): void {

    this.movieList = this.data.movieList;

    this.reviewForm = this.formBuilder.group({
      id: [''],
      comment: ['', Validators.required],
      commentator: ['', Validators.required],
      createAt: ['', Validators.required],
      movieId: ['', Validators.required],
    })

    if (this.data.id) {
      this.reviewForm.controls['id'].setValue(this.data.id)
      this.reviewForm.controls['comment'].setValue(this.data.comment)
      this.reviewForm.controls['commentator'].setValue(this.data.commentator)
      this.reviewForm.controls['createAt'].setValue(this.data.createAt)
      this.reviewForm.controls['movieId'].setValue(this.data.movie.id)
    }

    this.titleModal = this.data.titleModal
    this.buttonModal = this.data.buttonModal
  }

  addReview() {
    console.log(this.reviewForm.value)
    if (this.reviewForm.valid) {
      this.apiReview.saveOrUpdateReview(this.reviewForm.value).subscribe(
        {
          next: (res) => {
            // console.log(res)
            this.reviewForm.reset()
            this.dialogRef.close(true)
          },
          error: (err) => {
            // console.log(err)
          }
        }
      )
    }
    this.dialogRef.close(false)
  }

}
