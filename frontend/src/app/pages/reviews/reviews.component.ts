import {Component, OnInit, ViewChild} from '@angular/core';

import Swal from 'sweetalert2'
import {MatTableDataSource} from "@angular/material/table";
import {Review} from "../../interface/review";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {MatDialog} from "@angular/material/dialog";
import {ReviewService} from "../../services/review.service";
import {DialogReviewComponent} from "./dialogReview/dialogReview.component";
import {MovieService} from "../../services/movie.service";
import {Movie} from "../../interface/movie";


@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.scss']
})
export class ReviewsComponent implements OnInit {

  displayedColumns: string[] = ['id', 'comment', 'commentator', 'createAt', 'movie', 'actions'];
  dataSource: MatTableDataSource<Review> = new MatTableDataSource();

  dataMovie: Array<Movie> = [];

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(private dialog: MatDialog, private reviewService: ReviewService, private movieService: MovieService) {
  }

  ngOnInit(): void {
    this.listReview()
    this.listMovie()
  }


  agregarReview() {
    let settings = {
      titleModal: 'Agregar review',
      buttonModal: 'Agregar',
    }

    this.dialog.open(DialogReviewComponent, {
      width: '30%',
      data: {"movieList": this.dataMovie, ...settings}
    }).afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      result ? this.listReview() : null
    })
  }

  public listMovie() {
    this.movieService.listMovie().subscribe({
      next: (movies: any) => {
        this.dataMovie = movies.data;
        // console.log(this.dataMovie)
      },
      error: (error) => {
        // console.log(error)
      },
    })
  }

  listReview() {
    this.reviewService.listReview().subscribe({
      next: (reviews: any) => {
        let {data} = reviews;
        // console.log(reviews);
        // console.log(reviews.data);
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error: (error) => {
        // console.log(error)
      },
    })
  }

  editReview(review: Review) {
    let settings = {
      titleModal: 'Editar review',
      buttonModal: 'Editar',
    }

    this.dialog.open(DialogReviewComponent, {
      width: '30%',
      data: {...review, ...settings, "movieList": this.dataMovie}
    }).afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      result ? this.listReview() : null
    })
  }

  deleteReview(review: Review) {

    Swal.fire({
      title: '¿Estás seguro?',
      text: `¡No podrás revertir esta acción!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Eliminar'
    }).then((result) => {
      if (result.value) {
        this.reviewService.deleteReview(review.id).subscribe({
          next: (reviews: any) => {
            // console.log(reviews);
            this.listReview()
          },
          error: (error) => {
            // console.log(error)
          }
        })

        Swal.fire('Deleted!', 'Your file has been deleted.', 'success')
      }
    })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
