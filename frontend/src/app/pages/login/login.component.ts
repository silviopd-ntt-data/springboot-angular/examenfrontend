import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UsuarioService} from "../../services/usuario.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm !: FormGroup

  constructor(private formBuilder: FormBuilder, private apiUsuario: UsuarioService, private router: Router
  ) {
  }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  iniciarSesion() {
    if (this.loginForm.valid) {
      this.apiUsuario.iniciarSesion(this.loginForm.value).subscribe({
        next: (data) => {
          data.data ?
            this.router.navigate(['/movie'])
            : null
        },
        error: (err) => {
          console.log(err)
        }
      })
    }
  }

}
