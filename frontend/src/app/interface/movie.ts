export interface Movie {
  id?: Number;
  name: String;
  description: String;
  premiereDate: Date;
  premiere: Boolean;
}

